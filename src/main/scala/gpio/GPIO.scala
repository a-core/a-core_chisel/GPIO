// SPDX-License-Identifier: Apache-2.0

// Chisel module GPIO
// Inititally written by Verneri Hirvonen verneri.hirvonen@aalto.fi, 2021-07-08
package gpio

import chisel3._
import chisel3.util._
import chisel3.experimental._
import dsptools.{DspTester, DspTesterOptionsManager, DspTesterOptions}
import a_core_common._

/** IO definitions for GPIO */
class GPIOIO extends Bundle {
  val mode = Input(Bool()) // true = inputMode, false = outputMode
  val in = Input(UInt(8.W))
  val out = Output(UInt(8.W))
}

// BusIface connections for reference
// val addr  = Input(UInt(XLEN.W))
// val wdata = Input(UInt(XLEN.W))
// val rdata = Output(UInt(XLEN.W))
// val ren   = Input(Bool())
// val wen   = Input(Bool())
// val op_width = Input(MemOpWidth())

/** GPIO module with 8 hardcoded parallel outputs
  * @param proto type information
  * @param n number of elements in register
  */
class GPIO(XLEN: Int = 32, location: String = "h4000_0000") extends MultiIOModule with BusDevice {
  val io = IO(new GPIOIO)
  val bus_io = IO(new BusDeviceIO(XLEN=XLEN))

  // output register
  val register = RegInit(0.U(8.W))

  // default output values
  bus_io.bulk.rdata := 0.U

  when (io.mode) {
    // memory map register to given location
    when (bus_io.sel && bus_io.bulk.wen) {
      // only allow one byte wide writes for now
      when (bus_io.bulk.addr === location.U && bus_io.bulk.op_width === MemOpWidth.BYTE) {
        register := io.in 
      }
    }

    // read
    when (bus_io.sel && bus_io.bulk.ren) {
      when (bus_io.bulk.addr === location.U && bus_io.bulk.op_width === MemOpWidth.BYTE) {
        bus_io.bulk.rdata := io.in
      }
    }

  } .otherwise {
    // memory map register to given location
    when (bus_io.sel && bus_io.bulk.wen) {
      // only allow one byte wide writes for now
      when (bus_io.bulk.addr === location.U && bus_io.bulk.op_width === MemOpWidth.BYTE) {
        register := bus_io.bulk.wdata
      }
    }

    // read
    when (bus_io.sel && bus_io.bulk.ren) {
      when (bus_io.bulk.addr === location.U && bus_io.bulk.op_width === MemOpWidth.BYTE) {
        bus_io.bulk.rdata := register
      }
    }
  }

  io.out := register
}

/** This gives you verilog */
object GPIO extends App {
  chisel3.Driver.execute(args, () => new GPIO)
}

/** This is a simple unit tester for demonstration purposes */
class UnitTester(c: GPIO) extends DspTester(c) {
  // Tests are here 
  /*
  poke(c.io.A(0).real, 5)
  poke(c.io.A(0).imag, 102)
  step(5)
  fixTolLSBs.withValue(1) {
    expect(c.io.B(0).real, 5)
    expect(c.io.B(0).imag, 102)
  }
  */
}

/** Unit test driver */
object UnitTestDriver extends App {
  iotesters.Driver.execute(args, () => new GPIO) {
    c => new UnitTester(c)
  }
}
