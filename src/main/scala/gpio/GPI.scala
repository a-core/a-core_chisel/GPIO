// SPDX-License-Identifier: Apache-2.0

// Chisel module GPI
// Inititally written by Verneri Hirvonen verneri.hirvonen@aalto.fi, 2021-07-08
package gpio

import chisel3._
import chisel3.util._
import chisel3.experimental._
import dsptools.{DspTester, DspTesterOptionsManager, DspTesterOptions}
import a_core_common._
import scala.math._
import scala.collection.mutable.ArrayBuffer

/** IO definitions for GPI */
class GPIIO(val bits: Int = 8) extends Bundle {
  val in = Input(UInt(bits.W))
}

// BusIface connections for reference
// val addr  = Input(UInt(XLEN.W))
// val wdata = Input(UInt(XLEN.W))
// val rdata = Output(UInt(XLEN.W))
// val ren   = Input(Bool())
// val wen   = Input(Bool())
// val op_width = Input(MemOpWidth())

/** GPI module with 8 hardcoded parallel outputs
  * @param proto type information
  * @param n number of elements in register
  */
class GPI(XLEN: Int = 32, val begin: BigInt = 1073741824, bits: Int = 8) extends MultiIOModule with BusDevice {
  val io = IO(new GPIIO(bits=bits))
  val bus_io = IO(new BusDeviceIO(XLEN=XLEN))
  val bytes = (BigDecimal(bits/8.0).setScale(0, BigDecimal.RoundingMode.CEILING)).toBigInt
  val end = begin + bytes
  val input_synced = RegNext(RegNext(io.in))

  val valid = RegInit(0.U(8.W))
  
  // input registers
  val in_reg_arr = ArrayBuffer[chisel3.UInt]()
  for (i <- BigInt(0) until bytes){
    in_reg_arr += RegInit(0.U(8.W))
  }
 
  // default output values
  bus_io.bulk.rdata := 0.U

  // write to valid register
  when (bus_io.sel && bus_io.bulk.wen) {
    when (bus_io.bulk.addr === begin.U) {
      valid := bus_io.bulk.wdata
    }
  }

  // synced io.in to input registers
  for (i <- BigInt(0) until bytes) {
    // read from io.in to input register when valid is not on (valid != 1.U)
    when (valid =/= 1.U) {
      in_reg_arr(i.intValue) := input_synced(7 + (8*i.intValue), 0 + (8*i.intValue))
    }
  }

  // read
  when (bus_io.sel && bus_io.bulk.ren) {
    //read from input registers
    for(i <- BigInt(0) until bytes){
      when (bus_io.bulk.addr === (begin.U + i.U + 1.U) && bus_io.bulk.op_width === MemOpWidth.BYTE) {
        bus_io.bulk.rdata := in_reg_arr(i.intValue)
      }
    }
    // read whole number from input array when begin=lock signal location is called. This returns 32-bit num
    when (bus_io.bulk.addr === begin.U) {
      bus_io.bulk.rdata := Cat(in_reg_arr.reverse.toSeq)
    }
  }
}

/** Companion object for [[GPI]] with additional constructor functions */
object GPI extends App{
  /** Define convenience methods for [[String]] type */
  implicit class StringHelpers(str: String) {
    /** Convert number from chisel string representation to BigInt */
    def toBigInt = {
      val (base, num) = str.splitAt(1)
      val radix = base match {
        case "x" | "h" => 16
        case "d" => 10
        case "o" => 8
        case "b" => 2
        case _ => throw new IllegalArgumentException(s"""Could not parse number from string "$str", invalid base '$base'""")
      }
      BigInt(num.filterNot(_ == '_'), radix)
    }
  }

  /** Alternative constructor for [[GPI]]
    *
    * @param begin First address of the memory range in Chisel string format
    */
  def apply(XLEN: Int, begin: String, bits: Int) = {
    new GPI(XLEN, begin.toBigInt, bits)
  }
  
  /** This gives you verilog */
  chisel3.Driver.execute(args, () => new GPO)
}

//Tested with theSyDeKick
/** This is a simple unit tester for demonstration purposes */
class UnitTesterI(c: GPI) extends DspTester(c) {
  // Tests are here 
  /*
  poke(c.io.A(0).real, 5)
  poke(c.io.A(0).imag, 102)
  step(5)
  fixTolLSBs.withValue(1) {
    expect(c.io.B(0).real, 5)
    expect(c.io.B(0).imag, 102)
  }
  */
}

/** Unit test driver */
object UnitTestDriverI extends App {
  iotesters.Driver.execute(args, () => new GPI) {
    c => new UnitTesterI(c)
  }
}
